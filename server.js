const express = require('express')
const bodyParser = require('body-parser')


const app = express()
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

var routes = require('./routes')
routes(app)

app.listen(4000,()=>{
    console.log(`server running started on port 4000`)
})